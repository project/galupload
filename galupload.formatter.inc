<?php

/**
 * @file
 * Theming and formatting functions for Upload Input Method
 */         
function theme_galupload_upimage($element) {
  return theme('galupload_common', $element, 'upimage');
}

function theme_galupload_common($element, $uptype = '') {
  $output = '';
  static $gallery_cache;
  
  foreach (galleryapi_preset_load() as $preset) {
    if ('galleryapi_upload_'. $uptype .'_'. $preset['name'] === $element['#formatter']) {
      $pid = $preset['pid'];
      break;
    }
  }
  
  $gallery_id = 'node-'. $element['#node']->nid;
  if ($pid && is_numeric($pid)) {
    if (!isset($gallery_cache[$gallery_id])) {
      $data = array();
      foreach ($element as $key => $item) {      
        if (is_numeric($key) && $item['#item']['provider'] && $item['#item']['value']) {
          $slide_id = $gallery_id .'-'. $key;
          $data[$slide_id] = TRUE;
        }        
      } 

      $gallery_cache[$gallery_id] = galleryapi_build_gallery($pid, $data);
    }
    $output .= $gallery_cache[$gallery_id];
  }

  return $output;
}
